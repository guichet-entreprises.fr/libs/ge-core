/**
 * 
 */
package fr.ge.core.log;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.ILoggerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import fr.ge.core.bean.constante.GestionnaireTraceConstantes;

/**
 * Gestionnaire de trace (technique et fonctionnel) dans le journal des événements.
 *
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public final class GestionnaireTrace implements ILoggerFactory {

  /** Le logger technique commun. */
  public static final Logger LOG_TECH = GestionnaireTrace.getLoggerTechnique();

  /** Le logger fonctionnel commun. */
  public static final Logger LOG_FONC = GestionnaireTrace.getLoggerFonctionnel();

  /** Map contenant les loggers technique et fonctionnnel. **/
  private Map < String, Logger > loggerMap;

  /** Objet singleton. **/
  private static class GestionnaireTraceHolder {
    /** La constante instance. */
    private static final GestionnaireTrace INSTANCE = new GestionnaireTrace();
  }

  /**
   * Constructeur de la classe et Initilisation des loggers pour les messages techniques et
   * fonctionnel de l'application.
   */
  private GestionnaireTrace() {
    setLoggerMap(new HashMap < String, Logger >());
    getLoggerMap().put(GestionnaireTraceConstantes.FONC.getValue(),
      LoggerFactory.getLogger(GestionnaireTraceConstantes.FONC.getValue()));
    getLoggerMap().put(GestionnaireTraceConstantes.TECH.getValue(),
      LoggerFactory.getLogger(GestionnaireTraceConstantes.TECH.getValue()));
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Logger getLogger(final String name) {
    return getLoggerMap().get(name);
  }

  /**
   * Retourne le logger fonctionnel.
   *
   * @return logger fonctionnel
   */
  public static Logger getLoggerFonctionnel() {
    return getInstance().getLogger(GestionnaireTraceConstantes.FONC.getValue());
  }

  /**
   * Retourne le logger technique.
   *
   * @return logger technique
   */
  public static Logger getLoggerTechnique() {
    return getInstance().getLogger(GestionnaireTraceConstantes.TECH.getValue());
  }

  /**
   * Instanciation du singleton du gestionnaire de trace.
   *
   * @return unique instance de GestionnaireTrace
   */
  public static GestionnaireTrace getInstance() {
    return GestionnaireTraceHolder.INSTANCE;
  }

  /**
   * Accesseur sur l'attribut {@link #loggerMap}.
   *
   * @return Map<String,Logger> loggerMap
   */
  public Map < String, Logger > getLoggerMap() {
    return loggerMap;
  }

  /**
   * Mutateur sur l'attribut {@link #loggerMap}.
   *
   * @param loggerMap
   *          la nouvelle valeur de l'attribut loggerMap
   */
  public void setLoggerMap(final Map < String, Logger > loggerMap) {
    this.loggerMap = loggerMap;
  }

  /**
   * Suppression de toutes les valeurs du header des messages.
   */
  public static void clearAll() {
    MDC.remove(GestionnaireTraceConstantes.KEY_DOSSIER.getValue());
    MDC.remove(GestionnaireTraceConstantes.KEY_UTILISATEUR.getValue());
  }
}
