/**
 * 
 */
package fr.ge.core.bean.constante;

/**
 * Constante liées au fichier.
 * 
 * @author $Author: FADUBOIS $
 * @version $Revision: 0 $
 */
// TODO AOL 23-06-2017 : Supprimer ou déplacer cette classe !!
public interface IFilePathConstante {

  /** prefix d'un fichier dont l'origine est : local. **/
  String PREFIX_ORIGIN_LOCAL = "file";

  /** prefix d'un fichier dont l'origine est : sftp. **/
  String PREFIX_ORIGIN_SFTP = "sftp";

  /** separateur de fichier prefere a File.separator. **/
  String FILE_SEPARATOR = "/";

  /** nom du répertoire contenant les archives sur le repertoire local */
  String FOLDER_LOCAL_ARCHIVE = "archive";

  /** extension utilisé pour identifier les dossiers traités par les batchs. */
  String EXTENSION_FINI = ".fini";

  /** extension zip. */
  String EXTENSION_ZIP = ".zip";

}
