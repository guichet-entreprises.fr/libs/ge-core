/**
 * 
 */
package fr.ge.core.bean;

/**
 * UserBean qui ne contient que les informations minimales ( Identifiant Utilisateur issue de
 * Tracker).
 * 
 * @author $Author: FADUBOIS $
 * @version $Revision: 0 $
 */
public class UserBean {

  /**
   * Identifiant Tracker de l'utilisateur.
   */
  private String id;

  /**
   * Constructeur de la classe.
   *
   * @param id
   *          : Identifiant Tracker de l'utilisateur
   */
  public UserBean(final String id) {
    super();
    this.id = id;
  }

  /**
   * Accesseur sur l'attribut {@link #id}.
   *
   * @return String identifiant
   */
  public String getId() {
    return this.id;
  }

  /**
   * Mutateur sur l'attribut {@link #id}.
   * 
   *
   * @param id
   *          la nouvelle valeur de l'attribut identifiant
   */
  public void setId(final String id) {
    this.id = id;
  }

}
