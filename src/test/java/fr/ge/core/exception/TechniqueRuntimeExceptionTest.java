/**
 * 
 */
package fr.ge.core.exception;

import static org.fest.assertions.Assertions.assertThat;

import org.junit.Test;

/**
 * La Classe de test de TechniqueException.
 * 
 * @author $Author: FADUBOIS $
 * @version $Revision: 0 $
 */
public class TechniqueRuntimeExceptionTest {

  /**
   * Methode test des constructeurs.
   * 
   */
  @Test
  public void techniqueRuntimeExceptionTest() {
    assertThat(new TechniqueRuntimeException("message").getMessage()).isEqualTo("message");
    assertThat(new TechniqueRuntimeException("message", new Throwable()).getMessage()).isEqualTo("message");
    assertThat(new TechniqueRuntimeException(new Throwable("message"))).isNotNull();

    TechniqueRuntimeException exception = new TechniqueRuntimeException("message");
    exception.setUuid("uuid");
    exception.setCode("code");
    assertThat(exception.getUuid()).isEqualTo("uuid");
    assertThat(exception.getCode()).isEqualTo("code");
  }

}
