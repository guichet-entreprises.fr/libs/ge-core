/**
 * 
 */
package fr.ge.core.exception;

import static org.fest.assertions.Assertions.assertThat;

import org.junit.Test;

/**
 * La Classe de test de TechniqueException.
 * 
 * @author $Author: FADUBOIS $
 * @version $Revision: 0 $
 */
public class TechniqueExceptionTest {

  /**
   * Methode test des constructeurs.
   * 
   */
  @Test
  public void techniqueRuntimeExceptionTest() {
    assertThat(new TechniqueException("message").getMessage()).isEqualTo("message");
    assertThat(new TechniqueException("code", "message").getMessage()).isEqualTo("message");
    assertThat(new TechniqueException("code", "message").getCode()).isEqualTo("code");
    assertThat(new TechniqueException("message", new Throwable()).getMessage()).isEqualTo("message");
    assertThat(new TechniqueException("code", "message", new Throwable()).getMessage()).isEqualTo("message");
    assertThat(new TechniqueException("code", "message", new Throwable()).getCode()).isEqualTo("code");
    assertThat(new TechniqueException(new Throwable("message"))).isNotNull();

    TechniqueException exception = new TechniqueException("message");
    exception.setUuid("uuid");
    exception.setCode("code");
    assertThat(exception.getUuid()).isEqualTo("uuid");
    assertThat(exception.getCode()).isEqualTo("code");
  }

}
