/**
 * 
 */
package fr.ge.core.util;

import static org.fest.assertions.Assertions.assertThat;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;

import org.junit.Test;

import fr.ge.core.bean.constante.IFilePathConstante;

/**
 * Classe de test des méthodes utilitaires {@link FilePathUtil}.
 *
 * @author $Author: amonsone $
 * @version $Revision: 0 $
 */
public class FilePathUtilTest {

  /**
   * Methode de test de l'attribut de la class.
   * 
   * @throws Exception
   *           : Exception
   */
  @Test
  public void testClass() throws Exception {
    final Constructor < ? > constructor = FilePathUtil.class.getDeclaredConstructor();
    assertThat(constructor.isAccessible()).isFalse();
    assertThat(Modifier.isPrivate(constructor.getModifiers())).isTrue();
    constructor.setAccessible(true);
    constructor.newInstance();
    assertThat(constructor.isAccessible()).isTrue();
    constructor.setAccessible(false);
  }

  /**
   * Test de la methode generateAbsoluteDirPath.
   *
   */
  @Test
  public void testGenerateAbsoluteDirPath() {

    // file://${ge_gent_dir_pj}/${utilisateur_id}/${numero_dossier}

    // test 1 : pas de préfixe
    String absoluteDirPath = FilePathUtil.generateAbsoluteDirPath(IFilePathConstante.PREFIX_ORIGIN_LOCAL, "ge_gent_dir_pj",
      "utilisateur_id", "numero_dossier");
    assertThat(absoluteDirPath).isEqualTo("file://ge_gent_dir_pj/utilisateur_id/numero_dossier");

  }

}
