/**
 * 
 */
package fr.ge.core.message;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.Locale;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;

/**
 * Classe de test AbstractMessageProviderTest.
 * 
 * @author $Author: FADUBOIS $
 * @version $Revision: 0 $
 */
public class AbstractMessageProviderTest extends AbstractMessageProvider {

  @InjectMocks
  private AbstractMessageProvider thiz = this;

  @Mock
  MessageSource messagesTraces;

  @Before
  public void init() {
    MockitoAnnotations.initMocks(this);
  }

  /**
   * Methode de test getMessage.
   */
  @Test
  public void getMessageTest() {

    // test passant
    Object[] arrayObject = new Object[1];
    arrayObject[0] = "C000000000";
    when(messagesTraces.getMessage("dossier.etat0", arrayObject, Locale.getDefault()))
      .thenReturn("Le dossier référence C000000000 est créé.");

    assertThat(thiz.getMessage("dossier.etat0", "C000000000")).isEqualTo("Le dossier référence C000000000 est créé.");

    // test non passant avec une exception
    when(messagesTraces.getMessage("dossier.etat0", arrayObject, Locale.getDefault()))
      .thenThrow(new NoSuchMessageException("erreur"));

    assertThat(thiz.getMessage("dossier.etat0", "C000000000")).isNull();
  }

}
