/**
 * 
 */
package fr.ge.core.bean.constante;

import static org.fest.assertions.Assertions.assertThat;

import org.junit.Test;

import fr.ge.core.bean.constante.GestionnaireTraceConstantes;

/**
 * Classe de test GestionnaireTraceConstantes.
 * 
 * @author $Author: FADUBOIS $
 * @version $Revision: 0 $
 */
public class GestionnaireTraceConstantesTest {

  /**
   * Methode de test pour toString.
   */
  @Test
  public void toStringTest() {
    assertThat(GestionnaireTraceConstantes.FONC.toString()).isEqualTo("fonc");
  }

}
